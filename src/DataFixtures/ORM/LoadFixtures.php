<?php

namespace App\DataFixtures\ORM;

use App\DataFixtures\CustomProviders\GenusProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Nelmio\Alice\Loader\NativeLoader;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $faker->addProvider(GenusProvider::class);
        $loader = new NativeLoader($faker);
        $objectSet = $loader->loadFile(__DIR__ . '/fixtures.yml');
        $genuses = $objectSet->getObjects();

        foreach ($genuses as $genus) {
            $manager->persist($genus);
        }

        $manager->flush();
    }
}
